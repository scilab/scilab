/* ====================================================================== */
/* Template testbox */
/* This file is released under the 3-clause BSD license. See COPYING-BSD. */
/* ====================================================================== */

package org.scilab.contrib.testbox;

public class Sum {
    public static double sum(double a, double b) {
        return a + b;
    }
}
